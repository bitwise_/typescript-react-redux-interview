import * as React from 'react';
import './App.css';
import fullLogo from './assets/images/brand-logo/large.png'
import partLogo from './assets/images/brand-logo/small.png'
import {Loader} from "./components/loader/Loader";
import {A, D, E, F, FMState, G, I} from "./constants";
import {StoreState} from "./types";
import {connect} from "react-redux";
import Message from './components/message/Message'
import {Dispatch} from "redux";
import * as actions from "./actions";
import {setNode} from "./actions";
import Counter from "./components/counter/Counter";
import QRCode from "./components/qrcode/QRCode";
import * as sdk from './sdk'

const AppTDBar = ({navElements, logoUrl}: { navElements: { label: string, action: (e: any) => void }[], logoUrl: string }) => (
    <div className="container-fluid px-5">
        <div className="row">
            <div className="col-6">
                <img src={logoUrl} alt=""/>
            </div>
            <div className="col-6">
                {
                    navElements && navElements.map((e, i) => (
                        <div key={i + '-nav'} className='col-auto float-right menu-item'
                             onClick={e.action}>
                            {e.label}
                        </div>)
                    )
                }
            </div>
        </div>
    </div>
)

interface Props {
    actualNode: FMState,
    setNode: (from: FMState, to: FMState, test: string) => void
}

interface State {
    counterMax: number
}

class App extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            counterMax: NaN
        }

        sdk.getNumber()
            .catch((err: any) => {
                this.props.setNode(this.props.actualNode, D, '');
            })
            .then(
                ((value: any) => {
                    this.setState({...this.state, counterMax: value});
                    this.props.setNode(this.props.actualNode, I, '');
                })
            )
    }

    public render() {
        return (
            <div>
                <header>
                    <AppTDBar navElements={
                        this.props.actualNode === I ? [{
                            label: 'Cancel',
                            action: (e:any) => this.props.setNode(this.props.actualNode, E, '')
                        }] : []
                    } logoUrl={fullLogo}/>
                </header>
                <main>
                    {
                        this.props.actualNode === A ?
                        (<Loader/>) : null
                    }
                    {
                        this.props.actualNode === I ?
                        (<div className='container p-5'>
                            <div className="row justify-content-center">
                                <div className="col-auto text-center">
                                    <Counter counterVal={this.state.counterMax}/>
                                </div>
                                <div className="col-auto text-center">
                                    <QRCode textToEncode='chainsidepay'/>
                                </div>
                            </div>
                        </div>) : null
                    }
                    {
                        [D, E, F, G].indexOf(this.props.actualNode) !== -1 ?
                        (<Message/>) : null
                    }
                </main>
                <footer>
                    <AppTDBar navElements={
                        [
                            {label: 'FAQ', action: e => window.location.reload()},
                            {label: 'Help', action: e => window.location.reload()},
                            {label: 'Cookie Policy', action: e => window.location.reload()},
                            {label: 'Privacy Policy', action: e => window.location.reload()},
                            {label: 'Terms', action: e => window.location.reload()}
                        ]
                    } logoUrl={partLogo}/>
                </footer>
            </div>
        );
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.SetNodeAction>) {
    return {
        setNode: (from: FMState, to: FMState, test: string) => dispatch(setNode(from, to, test))
    }
}

export function mapStateToProps({actualNode}: StoreState) {
    return {
        actualNode
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
