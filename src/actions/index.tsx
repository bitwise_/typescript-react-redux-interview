import {A_SET_NODE, FMState} from "../constants";

export interface SetNodeAction {
    type: string,
    from: FMState,
    to: FMState,
    testData: string
}

export function setNode(prevNode:FMState, nextNode: FMState, testData: string): SetNodeAction {
    return {
        type: A_SET_NODE,
        from: prevNode,
        to: nextNode,
        testData
    }
}