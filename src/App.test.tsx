import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from "./App";
import {Provider} from "react-redux";
import {createStore} from "redux";
import {StoreState} from "./types";
import {SetNodeAction} from "./actions";
import {stepNext} from "./reducers";
import {A} from "./constants";

const store = createStore<StoreState, SetNodeAction, any, any>(stepNext, {
    actualNode: A
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
