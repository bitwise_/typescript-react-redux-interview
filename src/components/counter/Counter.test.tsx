import * as React from 'react'
import {render, fireEvent, cleanup, waitForElement} from 'react-testing-library'
import Counter from "./Counter";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {stepNext} from "../../reducers";
import * as sdk from '../../sdk'
import {I} from "../../constants";

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup)

function renderWithRedux(
    ui: React.ReactNode,
    { initialState, store = createStore(stepNext, initialState) } : {initialState? : any, store?: any} = {}
) {
    return {
        ...render(<Provider store={store}>{ui}</Provider>),
        // adding `store` to the returned utilities to allow us
        // to reference it in our tests (just try to avoid using
        // this to test implementation details).
        store
    }
}

test('Counter component renders without crashing', async () => {
    // Arrange
    const initialCounterMaxValue = await sdk.getNumber();
    renderWithRedux(
        <Counter counterVal={initialCounterMaxValue}/>,
        {initialState: {actualNode: I}}
    )
})

test('Counter component works as described in README.md', async () => {
    // Arrange
    const initialCounterMaxValue = await sdk.getNumber();
    const {getByText, getByTestId, container, asFragment, store} = renderWithRedux(
        <Counter counterVal={initialCounterMaxValue}/>,
        {initialState: {actualNode: I}}
    )




    /*
        // Act
        fireEvent.click(getByText('Load Greeting'))

        // Let's wait until our mocked `get` request promise resolves and
        // the component calls setState and re-renders.
        // getByTestId throws an error if it cannot find an element with the given ID
        // and waitForElement will wait until the callback doesn't throw an error
        const greetingTextNode = await waitForElement(() =>
            getByTestId('greeting-text'),
        )

        // Assert
        expect(axiosMock.get).toHaveBeenCalledTimes(1)
        expect(axiosMock.get).toHaveBeenCalledWith(url)
        expect(getByTestId('greeting-text')).toHaveTextContent('hello there')
        expect(getByTestId('ok-button')).toHaveAttribute('disabled')
        // snapshots work great with regular DOM nodes!
        expect(container.firstChild).toMatchSnapshot()
        // you can also use get a `DocumentFragment`, which is useful if you want to compare nodes across render
        expect(asFragment()).toMatchSnapshot()
        */
})