import {Dispatch} from "redux";
import * as actions from "../../actions";
import {F, FMState, G} from "../../constants";
import {setNode} from "../../actions";
import {StoreState} from "../../types";
import {connect} from "react-redux";
import * as React from "react";
import Timeout = NodeJS.Timeout;
import minus from '../../assets/images/minus.png'
import plus from '../../assets/images/plus.png'
import './Counter.css'

interface State {
    countdown: number;
    interval: Timeout;
    counter: number;
}

interface Props {
    actualNode: FMState;
    redirect: (from: FMState, to: FMState, test: string) => void;
    counterVal: number;
}

class Counter extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            countdown: 60,
            interval: setInterval(() => this.decreaseCountdown(), 1000),
            counter: 0
        }

    }

    increaseCount(){
        if(this.state.counter+1 === this.props.counterVal){
            this.props.redirect(this.props.actualNode, F, '')
        }
        this.setState({...this.state, counter: this.state.counter+1})
    }

    decreaseCount(){
        this.setState({...this.state, counter: this.state.counter-1})
    }

    decreaseCountdown() {
        if (this.state.countdown - 1 === 0) {
            this.props.redirect(this.props.actualNode, G, '')
        } else {
            this.setState({...this.state, countdown: this.state.countdown - 1})
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.interval)
    }


    render() {
        return (
            <div className="custom-container">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <p className='bold'>Click plus and reach {this.props.counterVal}<br/>
                            before time expires!</p>
                        </div>
                    </div>
                    <div className="row justify-content-center my-3">
                        <div className="col-auto">
                            <div className={'button-sm ' + (this.state.counter === 0 ? 'disabled' : 'active')}
                            onClick={ev => this.state.counter !== 0 && this.decreaseCount()}>
                                <img src={minus}/>
                            </div>
                        </div>
                        <div className="col-auto">
                            <div className="button-md disabled">
                                {this.state.counter}
                            </div>
                        </div>
                        <div className="col-auto">
                            <div className={'button-sm active m-auto'}
                                 onClick={ev => this.increaseCount()}>
                                <img src={plus}/>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <p>Time left: {this.state.countdown} second{this.state.countdown !== 1 ? 's' : ''}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.SetNodeAction>) {
    return {
        redirect: (from: FMState, to: FMState, test: string) => dispatch(setNode(from, to, test))
    }
}


export function mapStateToProps({actualNode}: StoreState) {
    return {
        actualNode
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);