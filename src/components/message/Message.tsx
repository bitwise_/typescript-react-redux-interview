import * as React from "react";
import alert from '../../assets/images/alert.png'
import error from '../../assets/images/error.png'
import success from '../../assets/images/success.png'
import {D, E, F, FMState, G} from "../../constants";
import {connect} from "react-redux";
import {StoreState} from "../../types";
import {Dispatch} from "redux";
import * as actions from "../../actions"
import {setNode} from "../../actions";
import './Message.css'


const StatusMessage = (
    {state, redirect}: {
        state: E | D | F | G,
        redirect: (from: FMState, to: FMState, test: string) => void
    }) => {

    let icon;
    let status;
    let message;
    let colorClass;
    let restartBtnLbl;
    let restartBtnAct = (ev: any) => window.location.reload()

    switch (state) {
        case D:
            icon = error;
            status = 'Error'
            message = 'An error occurred'
            colorClass = 'error'
            break
        case E:
            icon = error;
            status = 'Cancelled'
            message = 'Nothing to do here anymore'
            colorClass = 'error'
            restartBtnLbl = 'Reset'
            break
        case F:
            icon = success;
            status = 'Completed'
            message = 'Oh yeah'
            colorClass = 'success'
            restartBtnLbl = 'Continue'
            restartBtnAct = (e:any) => window.location.href = 'http://example.com'
            break
        case G:
            icon = alert;
            status = 'Expired';
            message = 'Time\'s up';
            colorClass = 'alert';
            restartBtnLbl = 'Reset'
            break
        default:
    }

    return (
        <div className="container p-5">
            <div className="row justify-content-center">
                <div className="col-auto">
                    <img src={icon} alt=""/>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className={'col-auto ' + colorClass}>
                    {status}
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col-auto">
                    <p>{message}</p>
                </div>
            </div>
            {
                restartBtnLbl &&
                (
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <button className="button-md active"
                                    onClick={restartBtnAct}>
                                {restartBtnLbl}
                            </button>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export function mapDispatchToProps(dispatch: Dispatch<actions.SetNodeAction>) {
    return {
        redirect: (from: FMState, to: FMState, test: string) => dispatch(setNode(from, to, test))
    }
}


export function mapStateToProps({actualNode}: StoreState) {
    return {
        state: actualNode
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatusMessage);