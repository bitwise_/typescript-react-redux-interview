import * as React from 'react'
import {render, fireEvent, cleanup, waitForElement} from 'react-testing-library'
import QRCode from "./QRCode";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {stepNext} from "../../reducers";
import {I} from "../../constants";

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup)

function renderWithRedux(
    ui: React.ReactNode,
    { initialState, store = createStore(stepNext, initialState) } : {initialState? : any, store?: any} = {}
) {
    return {
        ...render(<Provider store={store}>{ui}</Provider>),
        // adding `store` to the returned utilities to allow us
        // to reference it in our tests (just try to avoid using
        // this to test implementation details).
        store
    }
}

test('QRCode component renders without crashing', async () => {
    // Arrange
    renderWithRedux(
        <QRCode textToEncode='chainsidepay'/>,
        {initialState: {actualNode: I}}
    )
})

