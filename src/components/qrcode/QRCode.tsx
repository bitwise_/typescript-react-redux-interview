import {Dispatch} from "redux";
import * as actions from "../../actions";
import {FMState} from "../../constants";
import {setNode} from "../../actions";
import {StoreState} from "../../types";
import {connect} from "react-redux";
import * as React from "react";
import * as qrcode from "qrcode";
import './QRCode.css'

interface State {
    renderQR: boolean,
    QRImgUrl: string
}

interface Props {
    setNode: (from: FMState, to: FMState, test: string) => void;
    actualNode: FMState,
    textToEncode: string
}

class QRCode extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            renderQR: false,
            QRImgUrl: ''
        }
        qrcode.toDataURL(this.props.textToEncode)
            .then((v: any) => this.setState({...this.state, QRImgUrl: v}))
    }


    render() {
        return (
            <div className="custom-container">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <p className='bold'>
                            {this.state.renderQR ?
                                'Here a nice QR code' : 'QR Codes are cool.\nWant to see one?'}
                            </p>
                        </div>
                    </div>
                    {this.state.renderQR ?
                        (
                            <div className="row justify-content-center">
                                <div className="col-auto">
                                    <img src={this.state.QRImgUrl}/>
                                </div>
                            </div>) :
                        ''
                    }
                    <div className="row justify-content-center">
                        <div className="col-auto">
                            <p className='link'
                               onClick={ev => this.setState({
                                   ...this.state,
                                   renderQR: !this.state.renderQR
                               })}>{this.state.renderQR ? 'Hide QR Code' : 'Show QR Code'}</p>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}

export function mapDispatchToProps(dispatch: Dispatch<actions.SetNodeAction>) {
    return {
        setNode: (from: FMState, to: FMState, test: string) => dispatch(setNode(from, to, test))
    }
}


export function mapStateToProps({actualNode}: StoreState) {
    return {
        actualNode
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QRCode);