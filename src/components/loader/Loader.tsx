import './Loader.css'
import * as React from "react";
import loadImg from '../../assets/images/loading.png'

export const Loader = () => (
    <div className='container py-5'>
        <div className="row justify-content-center">
            <div className="col-auto text-center">
                <img src={loadImg} className='rotate' alt=""/>
            </div>
        </div>
        <div className="row justify-content-center">
            <div className="col-auto text-center">
                Loading
            </div>
        </div>
    </div>
)