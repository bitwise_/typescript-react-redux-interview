//state machine nodes names from specs
export const A = 'LOADING'; //start node
export type A = typeof A;

export const I = 'INNER'; //internal nodes B - C
export type I = typeof I;

export const D = 'ERROR'; //end node
export type D = typeof D;

export const E = 'CANCELLED'; //end node
export type E = typeof E;

export const F = 'COMPLETED'; //end node
export type F = typeof F;

export const G = 'EXPIRED'; //end node
export type G = typeof G;

export const STATES = [A, I, D, E, F, G];

export type FMState = A | I | D | E | F | G ;
export type FMEdges = { [stateName in FMState]: { to: FMState, condition: string }[] }

export const EDGES: FMEdges = {
    [A]: [
        {to: I, condition: ''},
        {to: D, condition: ''},
        {to: E, condition: ''},
        {to: F, condition: ''},
        {to: G, condition: ''}
    ],
    [I]: [
        {to: A, condition: ''},
        {to: D, condition: ''},
        {to: E, condition: ''},
        {to: F, condition: ''},
        {to: G, condition: ''}
    ],
    [D]: [
        {to: A, condition: ''},
        {to: I, condition: ''}
    ],
    [E]: [
        {to: A, condition: ''},
        {to: I, condition: ''}
    ],
    [F]: [
        {to: A, condition: ''},
        {to: I, condition: ''}
    ],
    [G]: [
        {to: A, condition: ''},
        {to: I, condition: ''}
    ]
}

export const A_SET_NODE = 'A_SET_NODE';
export type A_SET_NODE = typeof A_SET_NODE;