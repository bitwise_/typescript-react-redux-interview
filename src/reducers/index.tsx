import { SetNodeAction } from '../actions';
import { StoreState } from '../types';
import * as constants from '../constants';


export function stepNext(state: StoreState, action: SetNodeAction): StoreState {

    if(action.from === action.to) return state;

    switch (action.type) {
        case constants.A_SET_NODE:
            if(action.from in constants.EDGES &&
                constants.EDGES[action.from]
                    .find( n => n.to === action.to && n.condition === action.testData) !== undefined) {
                return { ...state, actualNode: action.to };
            }

    }
    return state;
}